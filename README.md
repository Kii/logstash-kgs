KGS-Logstash has for goal to automatically retrieve all game results of a player on KGS

Installation in 8 steps :

	1) 	Have a working version of logstash, I used 1.4.2 that can be find there : http://logstash.net/ 
	
		Have a working version of elastic-search, I used 1.5.2  that can be find there : https://www.elastic.co/products/elasticsearch

	2) 	Update kgs-extract.conf, line 31, the path must be where you want the csv to be created

	4) 	Update extract-archives.sh, line 3 to line 6 the path to logstash and elasticsearch must match your own

	5) 	Update extract-archives.sh, line 34 the path of the csv must be the same as in step 2

	6) 	Update kgs-split.conf, line 25 the path of the temporary file that will stored a splitted version of the logs

	7) 	Update kgs-output.conf, line 4 the path of the file must be the same as in step 6

	8) 	Update kgs-output.conf, line 46 the path of the csv containing all datas


Launching the extraction : 

	The script will launch 3 logstash agents and an elasticsearch daemon :
		sh extract-archives.sh YOUR_PLAYER_NAME
		
	Once the script is done, you can then explore the game results with Kibana and close logstash agents.


Explanations : 

	How does it work ? Very simple :
	
	1) the script requests KGS website with appropriate user name
	
	2) the response is downloaded and parsed to extract from when does the player play on KGS
	
	3) all html file containing game results are downloaded
	
	4) all html file are modified so each occurence of a 'cross' is replaced by the letter 'x'
	
	5) since the html are almost one-lined then a logstash agent parse it to make an new line each new html line (delimited by the presence of a <tr> balisa)
	
	6) then another logstash agent retrieve each html line a extract the game result
	
	7) the game results are stored in a CSV file or can be passed to an elasticsearch server


Informations : 

	extract-archives.sh will create a folder by player containing all html files with 1 char alteration
	
