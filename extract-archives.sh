if [ ! -z "$1" -a "$1" != " " ]; then

	gnome-terminal -x sh -c "cd /home/vincent/Documents/elasticsearch-1.5.2/bin; ./elasticsearch" &
	gnome-terminal -x sh -c "cd /home/vincent/Documents/logstash-kgs; logstash agent -f kgs-extract.conf" &
	gnome-terminal -x sh -c "cd /home/vincent/Documents/logstash-kgs; logstash agent -f kgs-split.conf" &
	gnome-terminal -x sh -c "cd /home/vincent/Documents/logstash-kgs; logstash agent -f kgs-output.conf" &

	echo "Launching Logstash Agents and ElasticSearch..."
	sleep 60


	if [ ! -d "kgs-$1" ]; then
		echo "Creating folder to store informations of $1"
		mkdir kgs-$1
	else
		echo "The folder kgs-$1 already exists"
	fi

	filename=kgs-$1/lastmonth-$1.html

	if [ ! -f $filename ]; then
		echo "Fetching last month informations file kgs-$1/lastmonth-$1.html..."
		curl "https://www.gokgs.com/gameArchives.jsp?user=$1" > $filename
	fi

	#send the file via tcp to logstash kgs-extract
	nc localhost 4321 < $filename

	echo "Waiting for player.csv to be updated..."
	sleep 10

	#to work, the player.csv file needs to be the one output by kgs-extract
	#Logstash agents must be working to acheive this step
	playerInfo="$(cat /home/vincent/Documents/logstash-kgs/player.csv | grep $1)"
	playerName="$(echo $playerInfo | awk '{print $1}')"
	playerStartYear="$(echo $playerInfo | awk '{print $2}')"
	playerStartMonth="$(echo $playerInfo | awk '{print $3}')"

	thisYear="$(date +'%Y')"
	thisMonth="$(date +'%m' | sed 's/^0*//')"

	yearDiff="$((thisYear - playerStartYear))"
	echo "il y a $yearDiff années d'ecart"

	if [ $yearDiff -eq 0 ]
	then
		monthDiff="$((thisMonth - playerStartMonth))"
		yearToMonthDiff=0
	else
		yearToMonthDiff="$((12 * (yearDiff -1)))"
		monthDiff="$((thisMonth + (12 - playerStartMonth)))"
	fi

	realDiff="$((yearToMonthDiff + monthDiff))"

	echo "$realDiff = $yearToMonthDiff + $monthDiff"
	echo "il y a $realDiff mois d'ecart"

	j=0
	for i in $(seq $playerStartMonth $((playerStartMonth + realDiff)))
	do
		j="$((j+1))"
		if [ ! -f "kgs-$1/mois$j.html" ]; then
			curl "https://www.gokgs.com/gameArchives.jsp?user=$1&year=$playerStartYear&month=$i" > kgs-$1/mois$j.html
			#the html original pages contain this strange "cross" character that makes encoding a pain, so I replace it by the beautiful letter x
			sed -i 's/×/x/g' kgs-$1/mois$j.html
			sleep 5
		else
			echo "File kgs-$1/mois$j.html already exists"
		fi
	done

	folder=./kgs-$1/mois*
	for file in $folder
	do
		#send the file via tcp to logstash kgs-split
		nc localhost 1234 < $file
		echo sending $file to logstash
		sleep 2
	done
else
	echo "Veuillez renseigner un nom de joueur sur KGS !"
fi
